package me.sargunvohra.mcmods.iamverysmart;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import me.sargunvohra.mcmods.iamverysmart.config.ClientConfig;
import me.shedaniel.autoconfig.AutoConfig;

@SuppressWarnings("unused")
public class ModMenuCompat implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return (screen) -> AutoConfig.getConfigScreen(ClientConfig.class, screen).get();
    }
}
