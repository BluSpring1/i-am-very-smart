pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        maven(url = "https://maven.fabricmc.net/")
    }
}

rootProject.name = "i-am-very-smart"
