import com.matthewprenger.cursegradle.CurseProject
import com.matthewprenger.cursegradle.CurseRelation
import com.matthewprenger.cursegradle.Options
import net.fabricmc.loom.task.RemapJarTask

plugins {
    java
    idea
    `maven-publish`
    id("fabric-loom") version "0.12-SNAPSHOT"
    id("com.matthewprenger.cursegradle") version "1.4.0"
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

base {
    archivesName.set("i-am-very-smart")
}

repositories {
    mavenCentral()

    maven(url = "https://maven.fabricmc.net")
    maven(url = "https://maven.terraformersmc.com")
    maven(url = "https://maven.shedaniel.me/")
}

version = "2.1.1+mc1.19"
group = "me.sargunvohra.mcmods"

dependencies {
    minecraft("com.mojang:minecraft:1.19")
    mappings("net.fabricmc:yarn:1.19+build.2:v2")
    modImplementation("net.fabricmc:fabric-loader:0.14.7")

    modImplementation("net.fabricmc.fabric-api:fabric-api:0.55.3+1.19")

    modApi("me.shedaniel.cloth:cloth-config-fabric:7.0.72") {
        exclude(group = "net.fabricmc.fabric-api")
    }

    modApi("com.terraformersmc:modmenu:4.0.0")
}

val processResources = tasks.getByName<ProcessResources>("processResources") {
    inputs.property("version", project.version)

    filesMatching("fabric.mod.json") {
        filter { line -> line.replace("%VERSION%", "${project.version}") }
    }
}

val javaCompile = tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

val jar = tasks.getByName<Jar>("jar") {
    from("LICENSE")
}

val remapJar = tasks.getByName<RemapJarTask>("remapJar")

curseforge {
    if (project.hasProperty("curseforge_api_key")) {
        apiKey = project.property("curseforge_api_key")!!
    }

    project(closureOf<CurseProject> {
        id = "318163"
        releaseType = "release"
        addGameVersion("1.19")
        addGameVersion("Fabric")
        relations(closureOf<CurseRelation> {
            requiredDependency("fabric-api")
            embeddedLibrary("cloth-config")
            embeddedLibrary("auto-config-updated-api")
        })
        mainArtifact(file("${project.buildDir}/libs/${base.archivesBaseName}-$version.jar"))
        afterEvaluate {
            mainArtifact(remapJar)
            uploadTask.dependsOn(remapJar)
        }
    })

    options(closureOf<Options> {
        forgeGradleIntegration = false
    })
}
